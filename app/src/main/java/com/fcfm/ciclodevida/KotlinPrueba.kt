package com.fcfm.ciclodevida
/*
data class Mascota{
    val id: Int,
    var nombre: String?,
    val color: String

}*/

//FUNCIONES DE EXTENSIÓN:
/*fun Int.suma(b:Int){
    this + b
}*/



//Para heredar es con dos puntos :
//Ejemplo ( class KotlinPrueba : Mascota {
/*class KotlinPrueba {
    val gato: Mascota?=null

    fun suma(x:Int,y:Int):Int {
        val resultado= x+y
        return resultado
    }
    fun prueba(){
        val perro= Mascota(0,"Peneka","Gris")

        perro.nombre=gato?.nombre //si detecta que es null, gracias a ese ? evita esa línea para que no falle
        perro.nombre=gato!!.nombre//Si es null no importa si crashea o no
    }


    val miVariable = 0
    val miVariable2= "nombre"
    val miVariable3= false
    val miVariable4: Int=2
    //Val es constante. (No cambia su valor)
    //Var si cambia.
    var miVariable5=2

    //En kotlin siempre tienen que tener valor las variables
    //Pero si ocupas tener un null puedes:
    var miVariable6: Int? = null

    lateinit var miVariable7: String //O darle un valor después con "lateinit", solo para var

    fun main(){
        val numeros= listOf(1,2,3,4,5) //Esta lista es estática. No se puede agregar mas objetos.

        val textos = mutableListOf("nombre") //Con "Mutable" se pueden agregar mas elementos
        textos.add("Alejandra")
        textos.add("Raul")

        val numeros2=(1..10).toList() //Crea una lista con números del 1 al 10

        for (i in 0..5){} //For que recorre del 1 al 5
        for (i in 0 until textos.size){} //Con "until" recorres hasta toparte el último elemento y ahorrarte errores

        //Conversiones:
        miVariable.toFloat()
        miVariable as Float

        //IF
        var respuesta=""
        if (miVariable2=="nombre")
        {
            val a=1
            val b=a+2
            respuesta="Correcta"
        } else{
            val a=1
            val b= a+2
            respuesta="Incorrecta"
        }
        //ESE IF PUEDE PASAR A ESTO:
        var respuesta= if (miVariable2=="nombre")
        {
            val a=1
            val b=a+2
           "Correcta"
        } else{
            val a=1
            val b= a+2
            "Incorrecta"
        }

        //Switch---------------------------------
        when(miVariable){
            2->{}
            3->{}
            else->{}
        } //También se puede pasar a una variable, como en mi ejemplo de if

    } //No regresa nada

    fun main2():String{
        return: "nombre"
    } //Regresa un string

    fun main3():String="nombre"
    fun main4()="nombre"
}*/