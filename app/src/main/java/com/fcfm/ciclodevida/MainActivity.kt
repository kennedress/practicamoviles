package com.fcfm.ciclodevida

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    var listOrder = arrayListOf<Int>()
    var listaUsuario = arrayListOf<Int>()

    var listOrderS = arrayListOf<String>()
    var listaUsuarioS = arrayListOf<String>()

    //fun String.mostrarMensaje(context: Context){
    fun mostrarMensaje(msj: String){
        Toast.makeText(this,msj,Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        //supportActionBar!!.hide()

        disorderString()
        Log.w("Ciclo de vida","onCreate")
        //findViewById()
        //val miBoton: Button = findViewById(R.id.btnAzul)
        // 1 = Azul
        // 2 = Rojo
        // 3 = Verde
        // 4 = Amarillo

        btnAzul.setOnClickListener { checkList("Azul") }
        btnRojo.setOnClickListener {  checkList("Rojo") }
        btnVerde.setOnClickListener {  checkList("Verde") }
        btnAmarillo.setOnClickListener {  checkList("Amarillo") }
        btnVer.setOnClickListener {
            /*Intent:
                 Abrir un activity
                Comunicación con activity

                Intent Explícitos: Son los que abren activities dentro de nuestra aplicación actual
                Intent Implícitos: Son los aque abren activities externos de nuestra aplicación (Ejemplo: abrir la cámara desde otra app)
            */
            val miIntent= Intent(this,RespuestaActivity::class.java)
            miIntent.putExtra("nombre","Nallely")
            miIntent.putExtra("edad","23")
            miIntent.putStringArrayListExtra("lista", listOrderS)
            /*var i = 1
            listOrderS.forEach {
                miIntent.putExtra(i.toString(), it)
                i++
            }*/
            //startActivity(miIntent)

            startActivityForResult(miIntent,1)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            1->{
                //Ver combinación
                if(resultCode==Activity.RESULT_OK){
                    //El usuario hizo lo que queríamos en el otro ativity
                    val nombre = data?.getStringExtra("nombre")
                    mostrarMensaje(("Muy mal $nombre por hacer trampa!"))
                }
                else if (resultCode==Activity.RESULT_CANCELED){
                    //El usuario abortó la operación en el otro activity
                    mostrarMensaje("Muy bien, no hiciste trampa!")
                }
            }
            2->{
                //obtenemos imagen de la camara
            }
            3->{
                //Obtenemos un token de sesion de FB
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.w("Ciclo de vida","onStart()")
    }


    override fun onResume() {
        super.onResume()
        Log.w("Ciclo de vida","onResume()")

    }

    override fun onPause() {
        super.onPause()
        Log.w("Ciclo de vida","onPause()")

    }

    override fun onStop() {
        super.onStop()
        Log.w("Ciclo de vida","onStop()")

    }

    override fun onRestart() {
        super.onRestart()
        Log.w("Ciclo de vida","onRestart()")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.w("Ciclo de vida","onDestroy()")

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.w("Ciclo de vida","onRestoreInstanceState()")

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        Log.w("Ciclo de vida","onSaveInstanceState()")

    }

    private fun disorderInt(){
        // 4 numeros en donde cada número representará un color
        // 0 = Azul
        // 1 = Rojo
        // 2 = Verde
        // 3 = Amarillo
        listaUsuario.clear()
        listOrder.clear()
        tvResultado.text = ""
        val listaOrdenada = arrayListOf(1,2,3,4)
        var i = 0
        var agrega = true
        do {
            i = listaOrdenada.random()

            // se llama it o le puedes poner otro nombre como: valor: Int ->
            listOrder.forEach{
                if(it == i){
                    agrega = false
                }
            }

            if(agrega) {
                listOrder.add(i)
                listaOrdenada.remove(i)
            }
        }while (listOrder.count() < 4)

        mostrarMensaje(listOrder.toString())
    }

    private fun disorderString(){
        listaUsuarioS.clear()
        listOrderS.clear()
        tvResultado.text = ""
        val listaOrdenada = arrayListOf("Azul","Rojo","Verde","Amarillo")
        var i = "i"
        var agrega = true
        do {
            i = listaOrdenada.random()

            // se llama it o le puedes poner otro nombre como: valor: Int ->
            listOrderS.forEach{
                if(it == i){
                    agrega = false
                }
            }

            if(agrega) {
                listOrderS.add(i)
                listaOrdenada.remove(i)
            }
        }while (listOrderS.count() < 4)

        mostrarMensaje(listOrderS.toString())
    }

    private fun checkList(valor: String){
        listaUsuarioS.add(valor)
        if(listaUsuarioS.count() == 4){
            if(listOrderS == listaUsuarioS){
                tvResultado.text = "ganaste"
            }else{
                tvResultado.text = "No ganaste"
            }

            val ad = AlertDialog.Builder(this@MainActivity)
            ad.setTitle(tvResultado.text)
            ad.setMessage("Volver a jugar?")
            ad.setPositiveButton("Si" , ({ _, _ ->
                disorderString()
            }))
            ad.setNegativeButton("No", ({ _, _ ->
                finish()
            }))
            ad.show()
        }
    }
}
