package com.fcfm.ciclodevida

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_respuesta.*

class RespuestaActivity : AppCompatActivity() {

    var hizoTrampa:Boolean=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_respuesta)

        val name = intent.getStringExtra("nombre")
        val edad = intent.getIntExtra("edad", 0)


        /*val lista = arrayListOf<Int>()

        val i = 1
        for (i in 1 .. 4){
            lista.add(intent.getIntExtra(i.toString(), 0))
        }*/

        val lista = intent.getStringArrayListExtra("lista")

        btnCancelar.setOnClickListener {
            hizoTrampa=false
            setResult(Activity.RESULT_CANCELED)
            finish() }

        btnHacerTrampa.setOnClickListener {
            hizoTrampa=true
            val intentRegreso= Intent()
            intentRegreso.putExtra("nombre", name)
            setResult(Activity.RESULT_OK,intentRegreso)
            tvCombinacion.text = lista.toString()

        }
    }
}
